class Subforwardlist{
public:
    Subforwardlist();
    ~Subforwardlist();

    bool push_back(int d);	//добавление элемента в конец недосписка
    int pop_back();
    bool push_forward(int d);	//добавление элемента в начало недосписка
    int pop_forward(); 	//удаление элемента из начала недосписка
    unsigned int size();
    bool push_where(unsigned int where, int d); //добавление элемента с порядковым номером where
    int erase_where(unsigned int where);	//удаление элемента с порядковым номером where
    void clear();
private:
    struct subforwardlist {
        int data;
        subforwardlist* next;
    };

    //##################################
        bool init(subforwardlist **sfl){ 	//инициализация пустого недосписка
            (*sfl) = NULL;
            return true;
        }
        bool push_back(subforwardlist **sfl, int d){ 	//добавление элемента в конец недосписка
            if ((*sfl) != NULL){
                subforwardlist* addres = (*sfl);
                while (addres->next != NULL){
                    addres = addres->next;
                }
                // subforwardlist* nx;
                subforwardlist* nx = new subforwardlist;
                nx->data=d;
                nx->next=NULL;
                addres->next = nx;
                // addres->next->data = d;
                // addres->next->next = NULL;
                return true;
            } else {
                (*sfl) = new subforwardlist;
                (*sfl)->data = d;
                (*sfl)->next = NULL;
                return true;
            }
            return false;
        }

        int pop_back(subforwardlist **sfl)
        {
            int temp_data = 0;
            if ((*sfl)!= NULL){
                subforwardlist* temp = *sfl;
                if ((temp->next)!= NULL){
                    while (temp->next->next != NULL)
                        temp = (temp->next);
                    temp_data = temp->next->data;
                    delete temp->next;
                    temp->next = NULL;
                    return temp_data;
                }
                else{
                    temp_data = temp->data;
                    delete temp;
                    (*sfl)=NULL;
                    return temp_data;
                }
            } else {
                return 0;
            }

        }
        bool push_forward(subforwardlist **sfl, int d){ 	//добавление элемента в начало недосписка
            subforwardlist* addres;
            addres = new subforwardlist;
            addres->data = d;
            addres->next = *sfl;
            *sfl = addres;
            return true;
        }
        int pop_forward(subforwardlist **sfl){ 	//удаление элемента из начала недосписка
            if(*sfl != NULL){
                subforwardlist* addres;
                addres = (*sfl)->next;
                int dat = (*sfl)->data;
                delete (*sfl);
                *sfl = addres;
                return dat;
            }
            return 0;
        }
        unsigned int size(subforwardlist  **sfl){	//определить размер недосписка
            if ((*sfl) != NULL){
                subforwardlist* addres = (*sfl);
                unsigned int ln = 1;
                while (addres->next != NULL){
                    addres = addres->next;
                    ln ++;
                }
                return ln;
            }
            return 0;
        }
        bool push_where(subforwardlist **sfl, unsigned int where, int d){ //добавление элемента с порядковым номером where
            if ((*sfl)!= NULL){
                subforwardlist* addres = (*sfl);
                // subforwardlist* addres_pr = NULL;
                if(where < size(sfl)){
                    while (where > 1){
                        // if(where == 1)
                        //     addres_pr = addres;
                        if (addres->next != NULL){
                            addres = addres->next;
                            where --;
                        }else{
                            return false;
                        }
                    }
                    subforwardlist* newElement;
                    newElement =  new subforwardlist;
                    newElement->data = d;
                    newElement->next = addres->next;
                    addres->next = newElement;
                    return true;
                } else {
                    while (addres->next != NULL){
                        addres = addres->next;
                    }
                    subforwardlist* newElement;
                    newElement =  new subforwardlist;
                    newElement->data = d;
                    newElement->next = NULL;
                    addres->next = newElement;
                    return true;
                }
            }
            return false;
        }
        int erase_where(subforwardlist **sfl, unsigned int where){	//удаление элемента с порядковым номером where
            if((*sfl) != NULL){
                subforwardlist* addres = (*sfl);
                if(where < size(sfl)){
                    while (where > 1){
                        if (addres->next != NULL){
                            addres = addres->next;
                            where --;
                        }else{
                            return 0;
                        }
                    }
                    int dat = addres->next->data;
                    subforwardlist* add = addres->next->next;
                    delete addres->next;
                    addres->next=add;
                    return dat;
                } else {
                    return 0;
                }
            }
            return 0;
        }

        void clear(subforwardlist  **sfl){	//очистить содержимое недосписка
            if ((*sfl) != NULL){
                subforwardlist* addres = (*sfl);
                subforwardlist* addres_pr = NULL;
                while (addres->next != NULL){
                    addres_pr = addres;
                    addres = addres->next;
                    delete addres_pr;
                }
                delete addres;
                *sfl = NULL;
            }
        }
    //##################################

    subforwardlist* root;
};

Subforwardlist::Subforwardlist(){
    // subforwardlist rt;
    init(&this->root);
}

bool Subforwardlist::push_back(int d){
    return push_back(&this->root, d);
}

int Subforwardlist::pop_back(){
    return pop_back(&this->root);
}

bool Subforwardlist::push_forward(int d){
    return push_forward(&this->root, d);
}

int Subforwardlist::pop_forward(){
    return pop_forward(&this->root);
}

unsigned int Subforwardlist::size(){
    return size(&this->root);
}

bool Subforwardlist::push_where(unsigned int where, int d){
    return push_where(&this->root, where, d);
}

int Subforwardlist::erase_where(unsigned int where){
    return erase_where(&this->root, where);
}

void Subforwardlist::clear(){
    clear(&this->root);
}

Subforwardlist::~Subforwardlist(){
    clear(&this->root);
}
